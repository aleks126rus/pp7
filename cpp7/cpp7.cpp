﻿#include <iostream>

class Stack
{
private:  
    int steck[10];    // Размер стека
    int top;           // Вершина стека

public:
    Stack()
    {
        top = 0;
    }
    void push(int var) // поместить в стек перменную
    {
        steck[++top] = var;
    }
    int pop() // берем из стека переменную
    {
        return steck[top--];
    }
};

int main()
{
    Stack s1;

    s1.push(11);
    s1.push(22);
    s1.push(33);
    std::cout << "1: " << s1.pop() << std::endl;
    std::cout << "2: " << s1.pop() << std::endl;
    std::cout << "3: " << s1.pop() << std::endl;
   
    return 0;
}








